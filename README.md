# API Security & Management

## Terminologies

- Oauth2
- OpenId

## Oauth2
- Oauth2 delegates user authentication to an independent service that stores user credentials and authorizes third-party applications to access shared information about users' accounts. 
- OAuth2 is used for giving your users access to data while protecting their account credentials.

### Oauth2 basic terms
- Resource owner: This role governs access to the resource. This access is limited by the scope of the granted authorization.
- Authorization grant: This grants permission for access.
    - access—authorization code
    - implicit
    - resource-owner password credentials
    - client credentials
- Resource server: This is a server that stores the owner’s resources that can be shared using a special token.
- Authorization server: This manages the allocation of keys, tokens, and other temporary resource access codes. It also has to ensure that access is granted to the relevant user.
- Access token: This is a key that allows access to a resource.

### The 4 types of Oauth2 grants:
- Password
- Client credential
- Access-Authorization code
- Implicit

### Demo

```bash
http://localhost:9999/oauth/authorize?response_type=token&client_id=piotr.minkowski&redirect_uri=http://example.com&scope=read

# Password grant
curl app-1:123456@localhost:9999/oauth/token -d grant_type=password -d username=rich -d password=1234

```